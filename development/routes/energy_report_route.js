/**
 * Author: Teemu Kanerva
 * Date: 22.08.2018
 * Route: /report
 * Description: Handles energy report API interface.
 * Returns JSON array of energy report data fetched from example data
 * Task 1. Top 5 charge points by charge counts
 * Task 2. Top 5 users by energy consumption
 * Task 3. Sum of energy from all charge actions
 * Task 4. Sum of all the charge times from all charge actions
 */

const fs = require('fs');
var FILE_NAME = './transactions.json';
var DEBUG_DATA  = './test_data.json';

//FILE_NAME = DEBUG_DATA;

/**
 * TASK 1
 * Author: Teemu Kanerva
 * Description: Fetch top five chargepoints based on charge count
 */
var getTopChargepointsByValue = () => {
  return new Promise((resolve,reject) => {
    fs.readFile(FILE_NAME,(err,full_result) => {
     
        let json = JSON.parse(full_result);
        let result = [];

        // Error handling
        if(err){
          reject();
          return;
        }

        let countArr = {};

        // Count charges by chargePointIds
        for(var i = 0; i < json.length; i++){
          if(countArr.hasOwnProperty(json[i].chargePointId)) {

            countArr[json[i].chargePointId] = countArr[json[i].chargePointId] + 1;

          } else {

            countArr[json[i].chargePointId] = 1;

          }
        }

        // Format data
        countArr = Object.entries(countArr);

        //Sort data by amount in descending order
        countArr.sort((a,b) =>{
          return b[1] - a[1];
        });

        // Only return top 5 values
        for(var i = 0; i < 5; i++){

          if(i < countArr.length){
            result.push({
              _id: countArr[i][0],
              amount: countArr[i][1]
            })
          }
        }

        resolve(result);
    });
  });
};

/**
 * TASK 2
 * Author: Teemu Kanerva
 * Description: Fetch top five clients by energy
 */
var getTopUsersByEnergy  = () => {
  return new Promise((resolve,reject) => {
    fs.readFile(FILE_NAME,(err,full_result) => {

        let json = JSON.parse(full_result);
        let result = [];
    
        // Error handling
        if(err){
          reject();
          return;
        }

        var countArr = {};

        // Count energy by clientId
        for(var i = 0; i < json.length; i++){
          if(countArr.hasOwnProperty(json[i].clientId)) {

            countArr[json[i].clientId] = countArr[json[i].clientId] + json[i].energy

          } else {

            countArr[json[i].clientId] = json[i].energy

          }
        }
        
        // Format data
        countArr = Object.entries(countArr);
        
        //Sort data by energy in descending order
        countArr.sort((a,b) =>{
          return b[1] - a[1];
        });

        // Only return top 5 values
        for(var i = 0; i < 5; i++){

          if(i < countArr.length){
            result.push({
              _id: countArr[i][0],
              energy: countArr[i][1]
            })
          }
        }
        resolve(result);
    });
  });
};

/**
 * TASK 3
 * Author: Teemu Kanerva
 * Description: Calculate sum of all the charges
 */
let getSumOfEnergyCharges  = () => {

  return new Promise((resolve,reject) => {
    fs.readFile(FILE_NAME,(err,full_result) => {

      let json = JSON.parse(full_result);
      let result = 0;

      // Error handling
      if(err){
        reject();
        return;
      }

      // Calculate sum of energy
      for(var i = 0; i < json.length; i++){

        result = result + parseInt(json[i].energy);
      }

      resolve(result);
    });
  });
};

/**
 * TASK 4
 * Author: Teemu Kanerva
 * Description: Calculated total duration for charges
 */
let calculatedSumOfTime = () => {

  return new Promise((resolve,reject) => {

    fs.readFile(FILE_NAME,(err,full_result) => {

      let json = JSON.parse(full_result);
      let result = 0;

      // Error handling
      if(err){
        reject();
        return;
      }

      // Time variables
      let t1;
      let t2;

      // Calculate total duration of all the charges
      for(var i = 0; i < json.length; i++){

        t1 = Date.parse(json[i].timestampStart);
        t2 = Date.parse(json[i].timestampStop);

        result = result + Math.abs((t2 - t1));

      }

      // convert milliseconds to seconds and return total sum of time in seconds
      resolve(result / 1000);
    });
  });

};

/**
 * Author: Teemu Kanerva
 * Description: Energy report API route function
 * Returns all promise based functions when they are finished running and returns result as JSON to client
 */
var getEnergyReport = (req,res) =>{

  let response = {
    totalEnergy: null,
    totalDuration: null,
    top5Chargepoints: null,
    top5Clients: null
  }

  Promise.all
  ([
    getSumOfEnergyCharges(),
    calculatedSumOfTime(),
    getTopChargepointsByValue(),
    getTopUsersByEnergy()
  ])
  .then((result) =>{

    response.totalEnergy = result[0];
    response.totalDuration = result[1]
    response.top5Chargepoints = result[2];
    response.top5Clients = result[3];

    res.status(200).send(response);
  })
  .catch((err) =>{
    res.status(200).send({
      message: 'invalid request',
      errorCode: err
    });
  })
};

module.exports = {
    getEnergyReport
}