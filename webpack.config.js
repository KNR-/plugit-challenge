var path    = require('path');
var webpack = require('webpack');
var fs      = require('fs');

var nodeModules = {};

fs.readdirSync('node_modules')
  .filter((x) => {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach((mod) => {
    nodeModules[mod] = 'commonjs ' + mod;
  });

module.exports = {
    target: 'node',
    entry: "./development/index.js",
    output: {
        filename: "./server.js",
    },
    externals: nodeModules,
}